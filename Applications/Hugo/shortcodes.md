To create a shortcode, you need to edit the files in your theme. In my case gitthings/archie. Then got to `layouts/shortcodes/`.

Create a file with the name of the shortcode you want to create, ie `fireside.html`.

Then put something in there like this:

```
<iframe src="https://player.fireside.fm/v2/{{ .Get 0 }}?theme=dark"
    width="{{ .Get 1 | default "560" }}" height="{{ .Get 2 | default "200" }}"
    frameborder="0" scrolling="no">
</iframe>

```

The .Get parts are the parameters that go into the shortcode to be expanded. The actual shortcode will look like this:

`{{ < fireside G3f9k8RZ+YUPBD4j > }}`


*Note that the spacing between the <> signs is important*


