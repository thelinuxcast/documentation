# Installation 

On openSUSE it is just `sudo zypper in hugo` 

# Create a site 

*Note this can be named anything.*

```
hugo new site quickstart
cd quickstart
git init
git submodule add https://github.com/theNewDynamic/gohugo-theme-ananke.git themes/ananke
echo "theme = 'ananke'" >> hugo.toml
hugo server

```

`hugo new site nameofsite`

# Creating Content 

* cd into the directory created in the previous command
* git init
* `git submodule add https://github.com/theNewDynamic/gohugo-theme-ananke.git themes/ananke`
* `echo "theme = 'ananke'" >> hugo.toml`
* `hugo server` 
  
Then a post

* `hugo new content content/posts/my-first-post.md`
* Then edit that post
* Change the draft status to `false`


---

Site configuration is done in the hugo.toml file.
To publish your site you simply run `hugo`


