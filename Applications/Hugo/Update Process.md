To update the website, aka publish a post, run the script `updatesite` which looks like this:

```
#!/usr/bin/env bash

# Set the right directories for your system
site="$HOME/mhome/Documents/Pages/hugo/TLC"


echo "Updating website!"
cd $site
git status
read -p "Git commit message: " COMMIT
git add .
git commit -m "$COMMIT"
git push
read -p "Done! Press any key to exit"

```

To update the site's theme, make changes to the git repo of the site (personally in gitthings/archie) and then run the sript `webtheme.sh`, which looks like this:

```

#!/bin/bash

theme="$HOME/mhome/Downloads/gitthings/archie"
site="$HOME/mhome/Documents/Pages/hugo/TLC"

cd "$theme"
git status
read -p "Git Commit Message: " COMMIT
git add .
git commit -m "$COMMIT"
git push

cd "$site"
git submodule update --force --recursive --init --remote
git status
read -p "Git Commit Message: " COMMIT
git add .
git commit -m "$COMMIT"
git push
read -p "DONE! Press Enter"

```
