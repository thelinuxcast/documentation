This creates a quick command to move current tab to another workspace.

1. Go to Settings > Quick Commands > Command Chains.
2. Click on  “Add Command Chain”.
3. Give the new chain a name.
4. Enter the following commands.
    - Use the  “Add Command” button in the top right corner of the first command to add the next one.
    - Focus Address Field
    - 7Delay (100)
    - Copy
    - Delay (1000)
    - Close Tab
    - Switch to Workspace #
    - New Tab
    - Delay (100)
    - Paste and Go
5. Go to Settings > Keyboard > Chains and/or Settings > Mouse >  New Gesture and find the chain you created.
6. Give the new chain a shortcut.
7. Go to a page you want to move and use the shortcut to test your new Command Chain.
8. Create the chain for each Workspace you have.


[Source](https://tips.vivaldi.net/tip-518/)
