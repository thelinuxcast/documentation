To install Davinci Resolve, Download package and unzip.
CD into the directory where the .run package resides 
./thatfile 
It will likely tell you that you do not have dependencies installed. Ensure that they are installed. It will probably not recognize that you actually installed them. So do sudo SKIP (unsure of actual command, it is given by the script) ./the .run file.

this will install Resolve.

It will likely not put the .desktop file in the correct location, but will instead put it on the Desktop. Move it to .local/share/applications/. 

Then run these lines: 

cd /opt/resolve/libs
sudo mkdir disabled-libraries
sudo mv libglib* disabled-libraries
sudo mv libgio* disabled-libraries
sudo mv libgmodule* disabled-libraries

This will put the libraries where they need to go.

So far this seems to only work with Bluefin/Fedora. It also works best with nvidia cards simply because it gives you access to more rendering options.
