This is the process for installing Plex on docker. I use portainer to do this, but this could easily be done with a docker-compose file. 

1. Name the container `plex`
2. The image comes from `linuxserver/plex:latest`. 
3. No ports.
4. Environment Variables:
    1. PUID/PGID to 1000
    2. TZ=America/Deteroit
    3. VERSION=latest
    4. PLEX_CLAIM = this can be found by going here: https://plex.tv/claim *Note that this code expires in 4 minutes so you may want to do this last*
5. Set Volumes:
    1. /config:/srv/dockerdata/plex
    2. /tv:/mnt/nfs/media/tvshows
    3. /movies:/mnt/nfs/media/Movies
    4. /music:/mnt/nfs/media/Music
6. Deploy container.
7. Go go to 192.168.86.xx:32400 to sign in and set up the server.


OR
 
 Docker compose it:
 
 ```
  ---
services:
  plex:
    image: lscr.io/linuxserver/plex:latest
    container_name: plex
    network_mode: host
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=America/Detroit
      - VERSION=latest
      - PLEX_CLAIM= claim-mqwNFysmhoFZZRN8gZ-H
    volumes:
      - /srv/dockerdata/plex/config:/config
      - /mnt/nfs/media/tvshows:/tv
      - /mnt/nfs/media/Movies:/movies
      - /mnt/nfs/media/Music:/music
      - /mnt/nfs/media/mommusic:/mommusic
    restart: unless-stopped
 
 ```
 
 
