This is specific to openSUSE. 

1. `sudo zypper install libvirt virt-manager`
2. `sudo usermod -aG libvirt matt`
3. `sudo systemctl enable --now  libvirtd.service`
4. Autostart the network to avoid errors: `sudo virsh net-autostart default`

## Create an ISO Pool

1. `mkdir /home/mhome/Downloads/isos'
2. In virt-manager, highlight kvm, then click Edit>Connection Details
3. Storage Tab
4. Click + at bottom left corner
5. Browse and then select directory created in step 1.
