To start the virtual network, 

first list the networks  `sudo virsh net-list --all`

Next,`sudo virsh net-start NAMEOFNETWORK` The name will likely be default.

If you want the network to autostart, then  `sudo virsh net-autostart default`

