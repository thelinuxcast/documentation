This is to setup Wireguard using mullvad on openSUSE.

1. Install `wireguard-tools`
2. Generate a VPN wireguard config file on mullvad's website - https://mullvad.net/account/wireguard-config?platform=linux
3. Download the file that generates and then copy or move it to `/ect/wireguard`
4. Make sure the permissions are correct for that directory and its contents: `sudo chown root:root -R /etc/wireguard && sudo chmod 600 -R /etc/wireguard`
5. Then to connect to wireguard: `wg-quick up se-mma-wg-001` Will require sudo password. Where se-mma-wg-001 is the name of the file you transferred to `/etc/wireguard` minus the .conf extension.
6. To disconnect: `wg-quick down se-mma-wg-001`, where se-mma-wg-001 is the name of the file from step 3 minus the .conf extension.

To verify your connection, `curl https://am.i.mullvad.net/connected`

Note that on Debian/Ubuntu, the Mullvad app is much better to use and offers more control over your connection. Unfortunately, it's not well maintained for openSUSE. (It is in the OBS, but I don't trust those packages.)

