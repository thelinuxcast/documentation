The vast majority of this setup is copied straight from Drew's setup. All credit goes to him. If you want to see this with pictures go to his guide [here](https://justaguylinux.com/documentation/software/geany/#initial-setup).

To change the color scheme, first download one from [this list](https://www.geany.org/download/themes/), then paste that theme in `$HOME/.config/geany/colorschemes`. To change the scheme within the app, go to `View> Change Colorscheme`. This only changes the colors for the editor. The rest of the UI is handled by your GTK theme. Sadly there is no gruvbox yet, but I will change this very soon.

### Preferences I Changed 

- Turn Sidebar off, turn shoew symbol list off and turn show document list off.
- Message window on the right.
- Toolbar>Show Toolbar, Append to the menu
- Editor>Line Wrapping, Smart Home key
- Editor>Completions>I turned on all of them, though will probably disable ''
- Editor>Display Turn off long line marker.
- Edit keybindings in View to show sidebar and message bar. This will allow you to see file tree and markdown preview.
- In Various, turn off scrollbars.

### Plugins
- geany-plugin-addons
- geany-plugin-git-changebar
- geany-plugin-markdown
- geany-plugin-spellcheck
- geany-plugin-treebrowser
- geany-plugin-insertnum
- geany-plugin-automark
- geany-plugin-vimode (of course)
- geany-plugin-export
- geany-plugin-macros


*As I said, most of these are straight from Drew's setup. As I customize it to be my own, I will update this document.*
