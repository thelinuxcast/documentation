This documentation applies to openSUSE. 

1. Install samba and samba-client.
2. `sudo systemctl enable --now smb nmb`
3. Edit /etc/samba/smb.conf

```
    [share]
        comment = Samba on openSUSE
        path = /home/matt/mhome/share
        read only = no
        browsable = yes
    [smssd]
        comment = Samba on openSUSE
        path = /smssd
        read only = no
        browsable = yes
```


4. `sudo systemctl restart smb nmb`
5. Run this - `nmcli connection modify enp6s0 connection.zone home`
6. In YaST, go to Firewall, and add samba and samba-client to allowed in the `HOME` group
7. Create SMB user: `sudo smbpasswd -a matt`
8. You will likely need to connect to this share in Windows manually. To do so

    1. Go to file manager then Network
    2. Right Click Network in the sidebar > "Add Map Network Drive"
    3. `\\IPADDRESSOFSERVER\NAMEOFSHARE`
    4. Enter password.

    
# Troubleshooting

- If you have issues, restart the service. 
- Check to see if the firewall is what is preventing the shares from being seen.
