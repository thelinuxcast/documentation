This is for the mysql version of Fresh RSS. It is not the db that I want to run, I may try the postgres one again.

This is the `.env` file you need. Note that I don't actually use any of these at the moment.

```
ADMIN_EMAIL=matt@matthewweber.xyz
ADMIN_PASSWORD=IsWaKi2003
ADMIN_API_PASSWORD=IsWaKi2003
# Published port if running locally
PUBLISHED_PORT=8080
# Database credentials (not relevant if using default SQLite database)
DB_HOST=freshrss-db
DB_BASE=freshrss
DB_PASSWORD=IsWaKi2003
DB_USER=freshrss

```

This is the `docker-compose.yml` file that I used.

```
version: "2.4"

volumes:
  data:
  extensions:

services:

  freshrss:
    image: freshrss/freshrss:latest
    # Optional build section if you want to build the image locally:
    build:
      # Pick #latest (stable release) or #edge (rolling release) or a specific release like #1.21.0
      context: https://github.com/FreshRSS/FreshRSS.git#latest
      dockerfile: Docker/Dockerfile-Alpine
    container_name: freshrss
    hostname: freshrss
    restart: unless-stopped
    logging:
      options:
        max-size: 10m
    volumes:
      - /srv/dockerdata/freshrss/data:/var/www/FreshRSS/data
      - /srv/dockerdata/freshrss/extensions:/var/www/FreshRSS/extensions
    ports:
      - 8080:80
    environment:
      TZ: America/Detroit
      CRON_MIN: '3,33'
      TRUSTED_PROXY: 172.16.0.1/12 192.168.0.1/16
    
```

Note the port and the changed volume names. 
Then go to IPADDRESS:8080 in the browser and finish setup there. With this current setup, use mysql/sqlite as the option in the database setup.

If moving from a previous installation of FreshRSS, you can export your feeds from FreshRSS by gong to > Feed Management > Import/Export.

This will save a .opml file, which you can import in the new installation on the same page as above.
