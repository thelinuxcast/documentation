Tautulli is a application that monitors Plex and provides statistics for Plex. 

To get it up and running, this is the dockercompose.yml:

```

version: '3'
services:
  tautulli:
    image: ghcr.io/tautulli/tautulli
    container_name: tautulli
    restart: unless-stopped
    volumes:
      - /srv/dockerdata/tautulli/config:/config
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=America/Detroit
    ports:
      - 8181:8181
```


To import an existing db:

1. Find the `.db` file in `/srv/dockerdata/tautulli/config/backups`
2. Copy that file to the new Backups directory
3. Go into Tautulli and then Settings>Import/Backup>And then find the '.db' in the location you stored it in.


