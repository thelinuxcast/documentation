Searx is very easy to install. 

In portainer:

1. Create new container.
2. In the image slot: `searxng/searxng`
3. Create ports: 8091:8080 (8080:8080 if you don't already use 8080, which you likely do)
4. create a volume on your server `/srv/dockerdata/searx`
5. In volumes on Portainer, create a volume bind pointing `/etc/searxng` to `/srv/dockerdata/searx`
6. In env variables, create two:
    1. BASE_URL= Then the URL of your instance, in this case https://searx.xxxxx.com
    2. INSTANCE_NAME = Whatever you want to name your instance
7. Change Restart policy to `unless stopped`.
8. Deploy container.

Then in Cloudflare, create a A Record for your subdomain from part 6.1.
Then point nginx to it with a proxy host. 

Source = https://docs.searxng.org/admin/installation-docker.html
