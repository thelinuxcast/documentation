Homepage is very simple to install:

```

version: "3.3"
services:
  homepage:
    image: ghcr.io/gethomepage/homepage:latest
    container_name: homepage
    ports:
      - 3000:3000
    volumes:
      - /srv/dockerdata/homepage/config:/app/config # Make sure your local config directory exists
      - /var/run/docker.sock:/var/run/docker.sock # (optional) For docker integrations
```

# Setup

Probably will never have to redo this from scratch again, so documentation is minimal, at least for now. 

Here is the documentation for the widgets: https://gethomepage.dev/latest/widgets/
