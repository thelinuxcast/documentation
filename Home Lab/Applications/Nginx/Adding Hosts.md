To Add a Host, and enable cloudflare,

1. Go to cloudflare, my profile, API keys, and create a new API key.
2. Go to NGINX, then SSL Certificates, create a new SSL Cert.
3. Use a wildcard like `*.thelinuxcast.org` as the URL.
4. Add Email Address
5. Select "Use DNS Challenge"
6. Select Cloudflare
7. Copy API Key from step 1 into text box.
8. Agree to terms.
9. Click Save.


Now, go to `Proxy Hosts`

1. Create new host.
2. Enter URL.
3. Enable proper options.
4. Add IP Address of the server that runs the service. Add appropriate port.
5. Go to SSL, select place holder SLL cert.
6. Click Force SSL
7. Click create. 
