# Portainer

This is for installing and setting Nginx up inside portainer using docker.

1. Create Container
2. Give the container a name.
3. In the image spot, put this:
    `jc21/nginx-proxy-manager:latest`
4. Create Ports 
    `80:80`
    `81:81`
    `443:443`
5. Create Volumes - In GUI, choose bind, not volume.
    - ./data:/data
    - ./letsencrypt:/etc/letsencrypt
(Change volume location paths to suit your needs)
    - I always use /srv/dockerdata
6. Change Restart policy to `unless-stopped`
7. Click "Deploy Container"

Then go to IPADDRESS:81

The default log in is:
   - User Name `admin@example.com`
   - Password  `changeme` 

Change email and then set new password.

You will need to port forward ports 80 and 443 on your router. This will be different depending on which router you have.

# Docker Compose or Portainer Stack

Alternatively, you can also use docker-compose or Portainer stacks (same thing) with this compose file:

```
version: '3.8'
services:
  app:
    image: 'jc21/nginx-proxy-manager:latest'
    restart: unless-stopped
    ports:
      - '80:80'
      - '81:81'
      - '443:443'
    volumes:
      - ./data:/data
      - ./letsencrypt:/etc/letsencrypt
```

Again, make sure that you change the volume locations for the host (which is the part in front of the :)
