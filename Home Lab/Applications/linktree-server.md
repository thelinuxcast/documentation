This docker container creates a linktree-style self hosted webpage. Original source [here](https://github.com/techno-tim/littlelink-server?tab=readme-ov-file)

This is the docker-compose.yml file that I used. 

```

version: "3.0"
services:
  littlelink-server:
    image: ghcr.io/techno-tim/littlelink-server:latest
    # dockerhub is also supported timothystewart6/littlelink-server
    # image: timothystewart6/littlelink-server:latest
    container_name: littlelink-server
    environment:
      - META_TITLE=Matthew Weber
      - META_DESCRIPTION=Historian | YouTuber | Writer | Reader | Blogger
      - META_AUTHOR=Matthew Weber
      - META_KEYWORDS=HomeLab, History, YouTube, Writing, Blogging, Reading
      - LANG=en
      - META_INDEX_STATUS=all
      - OG_SITE_NAME=Matthew Weber
      - OG_TITLE=Matthew Weber
      - OG_DESCRIPTION=The home of Matt Weber AKA The Linux Cast
      - OG_URL=https://thelinuxcast.org
      - OG_IMAGE=https://thelinuxcast.org/images/favicon.png
      - OG_IMAGE_WIDTH=400
      - OG_IMAGE_HEIGHT=400
      - THEME=Dark
      - FAVICON_URL=https://thelinuxcast.org/images/favicon.png
      - AVATAR_URL=https://thelinuxcast.org/images/favicon.png
      - AVATAR_2X_URL=https://thelinuxcast.org/images/favicon.png
      - AVATAR_ALT=TLC Profile Pic
      - NAME=Matthew Weber
      - BIO=Historian | YouTuber | Writer | Reader | Blogger
      # use ENV variable names for order, listed buttons will be boosted to the top
      - BUTTON_ORDER=YOUTUBE,GITHUB,DISCORD,FACEBOOK,PATREON,TWITCH,GEAR,DOCUMENTATION
      # you can render an unlimited amount of custom buttons by adding 
      # the CUSTOM_BUTTON_* variables and by using a comma as a separator.
      - CUSTOM_BUTTON_TEXT=Documentation,Recommended Gear
      - CUSTOM_BUTTON_URL=https://gitlab.com/thelinuxcast/documentation,https://l.technotim.live/gear
      - CUSTOM_BUTTON_COLOR=#000000,#000000
      - CUSTOM_BUTTON_TEXT_COLOR=#ffffff,#ffffff
      - CUSTOM_BUTTON_ALT_TEXT=Tech documentation site for my videos and more,Recommended Gear
      - CUSTOM_BUTTON_NAME=DOCUMENTATION,GEAR
      - CUSTOM_BUTTON_ICON=fas file-alt,fas fa-cog
      - GITHUB=https://gitlab.com/thelinuxcast
      - YOUTUBE=https://youtube.com/thelinuxcast
      - TWITCH=https://twitch.tv/thelinuxcast
      - DISCORD=https://discord.gg/yAH4w8tZ2k
      - FACEBOOK=https://facebook.com/thelinuxcast
      - PATREON=https://patreon.com/thelinuxast
      - FOOTER=Matthew Weber AKA The Linux Cast © 2024
    ports:
      - 8089:3000
    restart: unless-stopped
    security_opt:
      - no-new-privileges:true

```


