Nextcloud requires two docker containers. The first for Nextcloud, one for MariaDB. 

*Note, these instructions are for portainer only*

1. In portianer, create a new container.
2. Name it `nextcloud`
3. In the image slot put in `linuxserver/nextcloud:latest`
4. Create a port, if using nginx, user 444:443, instead of the 443:443 that the instructions say to use.
5. Create two volumes, data and config. Make sure to create these on the server you're hosting nextcloud on. Copy paths into the volumes section.
6. Create three ENV variables. PUID, PGID, and TZ. These should be set to 1000, 1000, and America/Detroit respectively.
7. Change restart policy to `unless stopped`
8. Deploy container.

To create the database:

1. Create new container
2. Name it `nextcloud_db`
3. In the image slot, put in 'linuxserver/mariadb:latest'
4. Put in ENV variables:
    1. PUID = 1000
    2. PGID = 1000
    3. TZ = America/Detroit
    4. MYSQL_ROOT_PASSWORD = Make a secure password
    5. MYSQL_DATABASE = dbname (probably nextcloud)
    6. MYSQL_USER = nextcloud
    7. MYSQL_PASSWORD = make a secure password
5. Make a Volume on your server for config. `/srv/dockerdata/nextcloud/database/config`
6. Put that volume as a bind in the container, point it towards `/config`
7. Set port to `3306:3306`
8. Set restart policy to `unless stop`

Go to https://IPADDRESS:444  *Note the s in https*
Create username and password. 
Click `Storage and Database`
Click MariaDB
Enter data from above. These are your env variables from above.
For the Database Host, use `IPADDRESS Of the SERVER`:3306
The install make take some time, so be patient.

Note that to set this up via nginx, you will need to do the following.

1. Set up proxy host.
2. on the server you are running the docker container on: `docker exec -it Nextcloud bash`
3. `cd /config/www/nextcloud`
4. `cd /config`
5. `nano config.php`
6. Add your domain to `array ()`
   ` 1 => 'nextcloud.thelinuxcast.org' `
7. restart container in portainer.



If needbe, this is the tut that I used -> https://www.youtube.com/watch?v=vAlgkKEvBMQ
