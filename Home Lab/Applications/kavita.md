This is the docker setup I have for Kavita.

```

services:
    kavita:
        image: jvmilazz0/kavita:latest
        container_name: kavita
        volumes:
            - /mnt/nfs/media/manga:/manga 
            - /mnt/nfs/media/comics:/comics          
            - /mnt/nfs/media/Books:/books            
            - /srv/dockerdata/kavita/config:/kavita/confg
        environment:
            - TZ=America/Detroit
        ports:
            - "5000:5000"
            
```
