# Setting Up NFS on Linux

#### From the server you want to share from

1. First make directories that you will share via nfs. You can name these and put them anywhere in /. 

        `mkdir /exports/{backups,stuff,media,hub}`

        I usually do this on my home server:
        
        ```
        sudo mkdir /backups #for backups
        sudo mkdir /mnt/Media #for Media
        sudo mdkir /mnt/sutff #for stuff
        sudo mkdir /hub #for the hub
        
        ```
        
2. Install nfs
        
        `sudo apt install nfs-kernel-server`
        
        *note that this will depend on the distro that you're using. This above is for Ubuntu/Debian*

3. Check to see if nfs is running, if not start it.
        
        `systemctl status nfs-kernel-sever`
        
        If it isn't: `systemctl enable --now nfs-kernel-server`
        
        *Once again, this may be different on different distros. Also, may require sudo*
        
        on openSUSE, this is just `nfs-server` and may require you to install extra components from YaST.
        
4. Now Edit `/etc/exports` to look like this:
    
    ```
        /backups     IPADDRESS(rw,sync,no_subtree_check,insecure)
        /mnt/stuff   IPADDRESS(rw,sync,no_subtree_check,insecure) IPADDRESS(rw,sync,no_subtree_check,insecure) 
        /mnt/Media   IPADDDESS(rw,sync,no_subtree_check,insecure) IPADDRESS(rw,sync,no_subtree_check,insecure) IPADDRESS(rw,sync,no_subtree_check,insecure) 
    
    ```
    
    *Note: You will need to add the IP Address of the machines that will be connecting to the NFS server to this file, for each mount. If you get an error on the client machine with autofs or with `mount` then it is likely that you forgot to add the IPADDRESS for the client at this step* 

5. Next restart the nfs server.

    `sudo systemctl restart nfs-kernel-server`
    
    and run 
    
    `sudo exportfs -rav`
    
#### On the Client

1. First check to see that the client can see the nfs mounts.

    `showmount --exports IPADDRESS`
    
2. Next, Make a directory to mount the NFS Shars

    `sudo mkdir /mnt/nfs`
    
3. Then make directories for each mount that you want to include:

    `sudo mkdir /mnt/nfs/backups`
   
    `sudo mkdir /mnt/nfs/stuff`
   
    `sudo mkdir /mnt/nfs/media`
   
    `sudo mkdir /mnt/nfs/hub`

4. Now you can mount the nfs shares.

    `sudo mount IPADDRESS:/backups /mnt/nfs/backups`

    `sudo mount IPADDRESS:/mnt/stuff /mnt/nfs/stuff`
     
    `sudo mount IPADDRESS:/mnt/Media /mnt/nfs/media`
    
    `sudo mount IPADDRESS:/hub /mnt/nfs/hub`
    
5. To Unmount, just use `sudo umount PATHOFMOUNT`

#### Get NFS Past Firewall

To get nfs to work on openSUSE specifically, you need to give it permission to bypass the firewall. 

```

sudo firewall-cmd --zone=public --add-service=nfs --permanent
sudo firewall-cmd --zone=public --add-service=rpc-bind --permanent
sudo firewall-cmd --zone=public --add-service=mountd --permanent
sudo firewall-cmd --reload

```

#### How to use AutoFS to Automount NFS Shares

*This is all done on the client*

1. Install `autofs`
    
    `sudo apt install autofs`
    
    *May be named differently on your distro. Is likely already installed*
    
2. Start the `autofs` service.

    `sudo systemctl enable --now autofs`
    
3. Edit `/etc/auto.master`
    
    Add this line: `/mnt/nfs /etc/auto.nfs --ghost --timeout=60`
    
    *Note: the first path is where your nfs shares will be mounted*

4. Make sure you remove the created directories from step #3 above. Autofs will create these, but will fail if they already exist.

5. Edit `/etc/auto.nfs`

    Add this:

    ```
    backups -fstype=nfs4,rw IPADDRESS:/backups
    media -fstype=nfs4,rw IPADDRESS:/mnt/Media
    hub -fstype=nfs4,rw IPADDRESS:/hub
    stuff -fstype=nfs4,rw IPADDRESS:/mnt/stuff
    
    ```
    Where the IPADDRESS in this case is the IP address of the server.
    
6. Restart `autofs`

    `sudo systemctl restart autofs`
    
    
This should get you so that your nfs shares are mounted only when you need them.
