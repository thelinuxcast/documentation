This is how I set up my portainer.

- Dependencies: Docker, Docker-Compose

`docker volume create portainer_data`

`docker run -d -p 8000:8000 -p 9443:9443 --name portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce:latest`


Create account after going to IPADDRESS:9443

Go to local>Environments and enter the IP Address in the correct feild. 
