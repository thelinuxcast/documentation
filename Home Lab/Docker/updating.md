#### Portainer

1. Make a Backup of your portainer. 
    - Settings > Back up Portainer
    - Download back up file.
2. `docker stop portainer`
3. `docker rm portainer`
4. `docker pull portainer/portainer-ce:latest`
5. `docker run -d -p 8000:8000 -p 9443:9443 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce:latest`

** Note: It is important to make sure the volumes are in the proper location. And note that you are likely to lose control over any stacks from within portainer. **
