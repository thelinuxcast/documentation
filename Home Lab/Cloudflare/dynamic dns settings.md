How to use ddns with cloudflare

1. Create a new an API token. 
   a. Profile > API Tokens > Create New Token > Scroll to the bottom and select "Create Custom Token"
   
   b. Give it a name
   
   c. Permissions
  
   ```
       Zone  - Zone Settings  - Read
       Zone  - Zone           - Read
       Zone  - DNS   		  - Edit

   ```
  
   d. Zone Resources
  
 
   ```
       Include -  All zones from account - Name of account
   ```
  
   e. Continue to summary.
   f. Create Token
   
   g. Copy token and save it somewhere.
   
2. Go to Portainer

   a. Create new stack
   
   b. Give the stack a name and then Paste in this docker compose.
   
   ```
   version: '2'
   services:
     cloudflare-ddns:
       image: oznu/cloudflare-ddns:latest
       restart: always
       environment:
         - API_KEY=xxxxxxx
         - ZONE=example.com
         - SUBDOMAIN=subdomain
         - PROXIED=false
   ```
   
   Change API_KEY, ZONE, SUBDOMAIN=freshrss or delete it if you're using the root of your domain just for your homelab, PROXIED=true.
   
   Add - PUID=1000 and - PGID=1000
   
   c. Deploy the stack
   
   For me I will need to change the A records to a CNAME record and point them to my default subdomain, the one that will be kept up to date by the docker container.
   
