# These are things I'd like to learn to do in the future:


- [ ] Use kubernetes
- [ ] self host jitsi
- [ ] create a movie library management and player app
- [ ] Get better at Bash
   - [ ] - make more bash scripts
   - [ ] - automate more stuff.
- [ ] Learn to package for the open build service
