To restore from a previous backup:

`borg mount matt@192.168.86.163:/mnt/backups/backups/borg borg`

where the borg at the end is the mountpoint. 
 
Dependencies: borg and an ssh key.
