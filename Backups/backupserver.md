This is my way of setting up borg on my docker server. This utilizes proxmox and borg.

1. Install borg. For Ubuntu/Debian this is `sudo apt install borgbackup borgmatic`
2. Create an sshkey on the server being backed up.
   `ssh-keygen -a 100 -t ed25519 -f ~/.ssh/id_ed25519 -C "{client-hostname}"`
3. Copy the public key. 
   `cat ~/.ssh/id_ed25519.pub`
4. Put that key in the `~/.ssh/known_hosts` file on the backup server.
5. Initialize a borg repo. If you want your borg repo to be at /backups/homelab then:
    `borg init --encryption=repokey matt@main:/backups/homelab`
6. Perform first backup.
    `sudo borg create matt@main:/backups/homelab::backupname /srv/dockerdata --progress`
 *I also have a backup script in my scripts repo named bu.sh, this can be altered to automate this provess.*

