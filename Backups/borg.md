I currently have a script to use borg. It will run the following command. 

borg create --stats --progress matt@main:/backups/borg::$date /home/matt


All this script does is replace the date variable with the current date. This is currently only being used for my main computer. I will see about using it for my homelab in the future.


# To break a lock, 

`borg break-lock matt@192.168.86.163:/mnt/backups/backups/borg`
