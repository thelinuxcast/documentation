* To list the number of updates available:
    `zypper lu | grep 'v |' | wc -l`

Handy to put in a script to be displayed on a bar.

