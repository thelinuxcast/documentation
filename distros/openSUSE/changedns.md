This is specific to openSUSE or distros that use NetworkManager instead of systemd to manage dns. To change dns, run this command, changing the two dns host names to fit your needs. 

`sudo nmcli con mod enp6s0 ipv4.dns "8.8.8.8 8.8.4.4"`


