sudo snapper create --desc "Snapshot After Fresh Installation"

sudo snapper list

# Rollback From Bootable Snapshots

Restoring your Linux system to previous known working state with Snapper is a quite helpful feature.

When you unknowingly misconfigured something or ended up with a broken system after system upgrade, you can easily rollback to previous state without losing any data.

Reboot your system. In the boot menu, choose "Start bootloader from a read-only snapshot" option.

￼
Start Bootloader From Read-only Snapshot
The list of snapshots is listed by date in the next window. The most recent snapshot is listed first. Select the snapshot you want to boot and hit Enter to login.

￼
Select The Snapshot To Boot
Log in to the system. Carefully check whether everything works as expected. Please note that you cannot write to any directory that is part of the snapshot. Data you write to other directories will not get lost, regardless of what you do next.

If everything is OK as expected, perform a rollback by running the following command from your Terminal.

$ sudo snapper rollback

From> https://ostechnix.com/btrfs-snapshots-snapper/
