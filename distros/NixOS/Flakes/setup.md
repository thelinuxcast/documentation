Flakes are fucking complicated and simple. It depends on who you ask, what they use flakes for, and how they set them up. This is my understanding, and it is very simple. To set up a flake, first you must enable them. To do this, put this in your nixos configuration file:

```
  #Enable Flakes

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

```

Then create the directory for your flake:

`mkdir myflake`

Then:

`nix flake init`

This will make the working directory the directory for your flake. 

This will also create a file flake.nix inside that directory. Think of this as the nixos configuration, but just for this flake.



# Commands for Flakes

`nix run` -> runs a packaged binary

`nix build` -> builds a package

`nix develop` -> actiates a nix dev shell

`nixos-rebuid` -> rebuilds the nixos system

`home-manager` -> rebuilds a home configuration
