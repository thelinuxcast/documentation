1. In order to get classnames in hyprland, run `hyprctl clients`.
2. To make file pickers float create this rule:
    - `windowrulev2 = float,class(xdg-desktop-portal-gtk)

    
# To Make your Volume nob or key work on Wayland/Hyprland

1. Get your sink
    `wpctl status`
2. In hyprland put this in your binds:
    ```
    bind = , XF86AudioRaiseVolume, exec, wpctl set-volume 64 1%+
    bind = , XF86AudioLowerVolume, exec, wpctl set-volume 64 1%-
    ```
    *Note that the sink has a tendency to change. Especially with the use of a kvm switch, which sucks.*
