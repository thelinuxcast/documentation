To bind audio controls to a keybinding, use

`pactl set-sink-volume 0 -5%`

and

`pactl set-sink-volume 0 +5%`

I use this in sxhkd.

*Dependency - pactl*

