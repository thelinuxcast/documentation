To dump current extension settings into a backup file:

`dconf dump /org/gnome/shell/extensions/dash-to-panel/ >dtp.conf`

To reset an extension completely:

`dconf reset -f /org/gnome/shell/extensions/dash-to-panel`
