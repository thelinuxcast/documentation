To install qtile extras, it is simple:

`pipx install qtile-extras`

Should work no matter how you installed qtile. 

See my gruvbox_shapes qtile config in My-Dots to see how to use qtile extras. Will expand this section in the future.
