To set up pywal, follow these steps.

1. Install pywal16: `pip3 install pywal16`
2. Put this in config.py

```
colors = []
cache='/home/matt/.cache/wal/colors'
def load_colors(cache):
    with open(cache, 'r') as file:
        for i in range(16):
            colors.append(file.readline().strip())
    colors.append('#ffffff')
    lazy.reload()
load_colors(cache)

```

Then make sure your colors[] are 0-15 otherwise your config won't load.

3. Then use this script to change the wallpaper.

```

#!/bin/bash

wal --cols16 -n -i "$@" -o "$HOME/mhome/Downloads/gitthings/scripts/dunstwal.sh" 
feh --bg-fil "$(< "${HOME}/.cache/wal/wal")"
#pywalfox update
qtile cmd-obj -o cmd -f restart

```
run this script with a image file as the argument. 

4. This should set the bar and qtile colors. 
5. Put this in your kitty.conf to change the colors in kitty. `include ~/.cache/wal/colors-kitty.conf`
6. You can also use the wal plugin to change the vim color scheme as well.
