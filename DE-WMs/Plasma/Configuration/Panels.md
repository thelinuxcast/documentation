In the panel, on various occasions, I use these plasmoids:

* [plasmusic-toolbar](https://github.com/ccatterina/plasmusic-toolbar)
* [Desktop Indicator](https://store.kde.org/p/2131462)
