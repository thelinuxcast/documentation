 This gives you rounded corners in KDE Plasma 6+.

1. `sudo zypper in git cmake gcc-c++ kf6-kconfigwidgets-devel kf6-kcmutils-devel kwin6-devel kf6-kwindowsystem-devel qt6-quick-devel qt6-core-private-devel`
2. 
  ``` 
git clone https://github.com/matinlotfali/KDE-Rounded-Corners
cd KDE-Rounded-Corners
mkdir build
cd build
cmake ..
cmake --build . -j
sudo make install
```

3. `sh ../tools/load.sh`
or log out and back in.

This can be enabled/disabled in Desktop Effects in the settings app.

*source: https://github.com/matinlotfali/KDE-Rounded-Corners*
