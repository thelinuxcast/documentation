This is how to set up the cron file initially:

*Dependencies: cronie or the like*

`crontab -e`

Insert this into the file:

```
# crontab -e
SHELL=/bin/bash
MAILTO=root@example.com
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
```
And then put in your cron jobs.
