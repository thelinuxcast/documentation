To make qt themes work on non-qt DEs,

First install kvantum-manager and qt5ct and qt6ct.

Set kvantum theme, change to kvantum-dark in both qt5/6ct apps.

Set 

`export QT_QPA_PLATFORMTHEME=qt5ct`

in `.profile`

Will likely require a reboot.
