In some instances, Electron apps will not work on Wayland properly. TO fix this you need to do something like this:

`flatpak run --socket=wayland com.discordapp.Discord --enable-features=UseOzonePlatform --ozone-platform=wayland`

Just replace the name of the app here, and then put that in the `exec` line of the .desktop file.

Flatpak keeps its .desktop files on openSUSE at `/var/lib/flatpak/exports/share/applications`. 


Note that this doesn't seem to work for all electron apps. A better way to do it, is to add this to your `.profile` file.
   
    `export ELECTRON_OZONE_PLATFORM_HINT=auto`
