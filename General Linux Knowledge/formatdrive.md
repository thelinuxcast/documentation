To format a drive in the terminal, 

1. use fdisk to format the drive.
2. in the terminal after writing, do `sudo mkfs -t ext4 /dev/sda1` 
   *note: remember to change the sda1 to the appropriate special device identifier*
