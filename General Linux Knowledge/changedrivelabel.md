To change the drive label on a connected drive (ext only) you can do this 

`sudo e2label /path/to/drive <label>`

Where /path/to/drive is probably something like /dev/sde.
