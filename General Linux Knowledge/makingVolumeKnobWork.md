In wayland, if the volume knob does not work out of the box (it should in a DE, probably won't in a WM), then bind this'

bind = , XF86AudioRaiseVolume, exec, wpctl set-volume 64 5%+
bind = , XF86AudioLowerVolume, exec, wpctl set-volume 64 5%-

Change to suit your WM. The above is for hyprland.

