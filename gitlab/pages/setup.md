This is how to set up a page, specifically for HUGO. 

1. Create a new, blank repository on gitlab.
2. In your HUGO project directory, add this to a `.gitlab-ci.yml` file:
    
    ```
    variables:
  DART_SASS_VERSION: 1.80.6
  HUGO_VERSION: 0.137.1
  NODE_VERSION: 20.x
  GIT_DEPTH: 0
  GIT_STRATEGY: clone
  GIT_SUBMODULE_STRATEGY: recursive
  TZ: America/Los_Angeles

image:
  name: golang:1.22.1-bookworm

pages:
  script:
    # Install brotli
    - apt-get update
    - apt-get install -y brotli
    # Install Dart Sass
    - curl -LJO https://github.com/sass/dart-sass/releases/download/${DART_SASS_VERSION}/dart-sass-${DART_SASS_VERSION}-linux-x64.tar.gz
    - tar -xf dart-sass-${DART_SASS_VERSION}-linux-x64.tar.gz
    - cp -r dart-sass/ /usr/local/bin
    - rm -rf dart-sass*
    - export PATH=/usr/local/bin/dart-sass:$PATH
    # Install Hugo
    - curl -LJO https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_extended_${HUGO_VERSION}_linux-amd64.deb
    - apt-get install -y ./hugo_extended_${HUGO_VERSION}_linux-amd64.deb
    - rm hugo_extended_${HUGO_VERSION}_linux-amd64.deb
    # Install Node.js
    - curl -fsSL https://deb.nodesource.com/setup_${NODE_VERSION} | bash -
    - apt-get install -y nodejs
    # Install Node.js dependencies
    - "[[ -f package-lock.json || -f npm-shrinkwrap.json ]] && npm ci || true"
    # Build
    - hugo --gc --minify
    # Compress
    - find public -type f -regex '.*\.\(css\|html\|js\|txt\|xml\)$' -exec gzip -f -k {} \;
    - find public -type f -regex '.*\.\(css\|html\|js\|txt\|xml\)$' -exec brotli -f -k {} \;
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

    ```
    
3. Do the following from the terminal
    
    ```
    git init
    echo "/public" >> .gitignore
    git add .
    git commit -m "Initial commit"
    git remote add origin SSH LINK to the REPO YOU MADE.
    git push -u origin master

    ```
4. Got to Build > Pipelines to see if HUGO successfully built.


Use a custom Domain.

1. Go to your gitlab repository.
2. Go to Deploy > Pages.
3. Click New Domain.
4. Add CNAME and TXT file to your DNS. Wait for DNS to propagate.
5. Wait for lets encrypt to get you a cert.


