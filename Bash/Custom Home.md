This is the start of me using a custom home directory. The first thine I always do is create the direcotry.

`mkdir mhome`

And then, I make it so that bash always treats this as the home directory, without it actually being the $HOME directory.

That is done with this bit of code:

```

function cd() {
    if [ $# -eq 0 ]; then
        builtin cd ~/mhome
    else
        builtin cd "$@"
    fi
}


```

inside your .bashrc. This will mean that CD will just work as CD if it is given an argument, if it does not recieve an argument, it will cd into my custom home directory.

Finally, I change `~/.config/xdg-dirs.dirs` so that it matches the new location of the Documents, Pictures, Videos, etc. directories.
 
If you use zoxide, add this instead:

```

cd() {
    if [ $# -eq 0 ]; then
        # Change to a default directory when no arguments are provided
        cd ~/mhome || return
    else
        # Fall back to the __zoxide_z function when arguments are present
        __zoxide_z "$@"
    fi
}

```





