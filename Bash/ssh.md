In order to get ssh working on my system, I first copy my ssh directory from a backup. This goes in $HOME. 

Once I do that, I run ssh-add, this initializes the ssh keys that already exist. 

Then I add `ssh-add` to my `.bashrc` file. This means that ssh will check for the passprhase on startup, and not require that password every time I use git or whatever.

There is also a dependency ssh-askpass that can be installed for window managers that gives polkit like password authentication at startup for ssh. On openSUSE, this package is called `openssh-askpass-gnome`.
